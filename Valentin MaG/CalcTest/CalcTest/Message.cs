﻿using System;
namespace CalcTest
{
	public enum MessageType
	{
		IsOk, 
		IsExit, 

	}

	public class Message
	{
		public MessageType MessageType { get; private set; }  
		public decimal? ReturnValue { get; private set; }
		public Message(decimal? _ReturnValue, MessageType _TypeOfMessage)
		{
			this.MessageType = _TypeOfMessage;
			this.ReturnValue = _ReturnValue; 
		}
	}
}
