﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;

namespace ParkingTest
{
	class Message
	{
		/// some comment !!!!
		///  
		public bool IsError { get; set; }
		public string StrMessage { get; set; }
		public Message(bool iserror)
		{
			IsError = iserror;
		}
		public Message(bool iserror, string errormessage)
		{
			IsError = iserror;
			StrMessage = errormessage;
		}
	}
	class Car
	{
		public string Number { get; protected set; }
		public int Size { get; protected set; }
		public Car(string number, int size)
		{
			Number = number;
			Size = size;
		}
	}
	class Parking
	{
		public string Location { get; }
		public int MaxNumberOfParkingSpace { get; }
		private readonly List<ParkingSpace> ParkingSpaces;
		public Parking(string location, int maxnumberofparkingspace)
		{
			if (maxnumberofparkingspace == 0) throw new InvalidOperationException("some error text");
			if (String.IsNullOrEmpty(location)) throw new InvalidOperationException("some error text");

			Location = location;
			MaxNumberOfParkingSpace = maxnumberofparkingspace;
			ParkingSpaces = new List<ParkingSpace>();
			for (int i = 1; i <= maxnumberofparkingspace; i++)
			{
				ParkingSpaces.Add(new ParkingSpace(i));
			}
		}
		public int NumberOfFree
		{
			get
			{
				return ParkingSpaces.Where(ps => ps.IsEmpty == true).Count();
			}
		}
		private List<ParkingSpace> ParkingSpaceByCarNumber(string carNumber)
		{
			return ParkingSpaces.Where(ps => ps.Car != null).Where(ps => ps.Car.Number == carNumber).ToList();
		}
		private ParkingSpace FreeParkingSpace
		{
			get
			{
				return ParkingSpaces.Where(ps => ps.IsEmpty == true).First();
			}
		}
		public Message ParkCar(string carNumber, int carSize)
		{
			Car car = new Car(carNumber, carSize);

			if (NumberOfFree == 0) { return new Message(true, "No free space"); }
			if (NumberOfFree < car.Size) { return new Message(true, "Car is too big!!!"); }

			for (int i = 1; i <= car.Size; i++)
			{
				FreeParkingSpace.ParkCar(car);
			}

			return new Message(false, "All ok - car parked");
		}
		public Message UnPark(string carNumber)
		{
			List<ParkingSpace> PS = ParkingSpaceByCarNumber(carNumber);
			if (PS.Count() == 0) { return new Message(true, "Car not found! Oooops!!"); }
			foreach (ParkingSpace ps in PS)
			{
				ps.UnParkCar();
			}

			return new Message(false, "All ok - car unparked!");
		}

	}
	class ParkingSpace
	{
		public int Number { get; set; }
		public Car Car { get; protected set; }
		public bool IsEmpty { get; protected set; }
		public ParkingSpace(int number)
		{
			Number = number;
			IsEmpty = true;
		}
		public void ParkCar(Car car)
		{
			Car = car;
			IsEmpty = false;
		}
		public void UnParkCar()
		{
			Car = null;
			IsEmpty = true;
		}
	}

}
