﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectTest
{
    class Program
    {
        static void Main(string[] args)
        {

            Object o1 = new object();
            Object o2 = new object();

            if (o1 == o2)
            {
                Console.WriteLine(true);
            }
            else
            {
                Console.WriteLine(false); 
            }


            if (o1.Equals(o2))
            {
                Console.WriteLine(true);
            }
            else
            {
                Console.WriteLine(false); 
            }



            Console.ReadLine(); 

        }
    }
}
