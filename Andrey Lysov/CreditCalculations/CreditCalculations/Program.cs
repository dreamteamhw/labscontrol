﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditCalculations
{
    class Program
    {
        static void Main(string[] args)
        {
            CreditCalculations();
        }

        public static void CreditCalculations()
        {
            double sum = enterNumber("Please enter credit sum");
            int period = enterNumber("Please enter credit period");
            int percentage = enterNumber("Please enter credit percentage");
            DateTime startDate = enterDate("Please enter start credit date (yyyy-MM-dd)");
            double onePayment = (double)sum / period;
            double percentsPerMonth = (double)percentage / 1200;

            for (int i = 0; i < period; i++)
            {
                startDate = startDate.AddMonths(1);
                Console.WriteLine(startDate.Date + "\t" + Math.Round(onePayment, 2) + "\t" + Math.Round(percentsPerMonth * sum, 2) + "\t" + Math.Round(sum, 2));
                sum -= onePayment;
            }
            Console.ReadLine();
        }

        public static int enterNumber(string presentation)
        {
            int value;
            Console.WriteLine(presentation);
            while (true)
            {
                if(int.TryParse(Console.ReadLine(), out value))
                {
                    break;
                }
                Console.WriteLine("Wrong input, please enter a number");
            }
            return value;
        }

        public static DateTime enterDate(string presentation)
        {
            Console.WriteLine(presentation);
            string date = Console.ReadLine();
            DateTime dataTime = DateTime.ParseExact(date, "yyyy-MM-dd",
                                              CultureInfo.InvariantCulture);
            return dataTime;
        }
    }
}
