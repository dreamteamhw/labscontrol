﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDCounter
{
    class Program
    {
        static void Main(string[] args)
        {
            bdDaysOfWeek();
            Console.ReadLine();
        }

        public static void bdDaysOfWeek()
        {
            DateTime bd = enterBD("Please enter date of yout birth day (yyyy-MM-dd)");
            const int MAX_AGE = 100;
            int mondaysCount = 0;
            int tuesdaysCount = 0;
            int wednesdaysCount = 0;
            int thursdaysCount = 0;
            int fridaysCount = 0;
            int saturdaysCount = 0;
            int sundaysCount = 0;
            for (int i = 0; i < MAX_AGE; i++)
            {
                bd = bd.AddYears(1);
                switch (bd.DayOfWeek)
                {

                    case DayOfWeek.Monday:
                        mondaysCount++;
                        break;
                    case DayOfWeek.Tuesday:
                        tuesdaysCount++;
                        break;
                    case DayOfWeek.Wednesday:
                        wednesdaysCount++;
                        break;
                    case DayOfWeek.Thursday:
                        thursdaysCount++;
                        break;
                    case DayOfWeek.Friday:
                        fridaysCount++;
                        break;
                    case DayOfWeek.Saturday:
                        saturdaysCount++;
                        break;
                    case DayOfWeek.Sunday:
                        sundaysCount++;
                        break;
                }
            }
            Console.WriteLine("You have {0} birthdays on Monday, {1} on Tuesday, {2} on Wednesday," +
                    " {3} on Thursday, {4} on Friday, {5} on Saturday, {6} on Sunday", mondaysCount, tuesdaysCount, wednesdaysCount, thursdaysCount, fridaysCount, saturdaysCount, sundaysCount);
        }

        public static DateTime enterBD(string precentation)
        {
            Console.WriteLine(precentation);
            string bd = Console.ReadLine();
            return DateTime.ParseExact(bd, "yyyy-MM-dd",
                                              CultureInfo.InvariantCulture);
        }
    }
}
