﻿using System;

namespace FirstHomeTask
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter 1(test) or 2(application), 3(Fibonachi array), "+
                "4(get number which devide to each of entered numbers), " +
                "5(odometr)");
            byte b = byte.Parse(Console.ReadLine());
            if(b == 1)
            {
                checkPrintingNumber654();
            } else if(b == 2)
            {
                int number = enterIntegerFromConsole();
                Console.WriteLine(printInteger(number));
            } else if (b == 3)
            {
                Console.WriteLine("enter length of the Fibonachi array and enter 0 for getting unlimited Fibonachi array");
                printFibonachiRow(enterIntegerFromConsole());
            } else if (b == 4)
            {
                Console.WriteLine("Please enter array length");
                int arrayLength = enterIntegerFromConsole();
                int[] intArr = intArray(arrayLength);
            } else if (b == 5)
            {
                Console.WriteLine("Plese enter length of the odometr");
                int odomLength = enterIntegerFromConsole();
                Console.WriteLine("Please enter length of the odometr rotor");
                int rotorLength = enterIntegerFromConsole();
                printOdometrWithParameters(odomLength, rotorLength);
                
            }
            Console.ReadLine();
        }

        public static int[] intArray(int arrayLength)
        {
            int[] intArr = new int[arrayLength];
            for(int i = 0; i < arrayLength; i++)
            {
                intArr[i] = enterIntegerFromConsole();
            }
            return intArr;
        }
        public static string printInteger(int number, string result = "", int i = 0)
        {
            if (number == 0)
            {
                return result;
            }

            string final = result;

            if(result.Length > 0)
            {
                final += " ";
            }

            if (number < 10)
            {
                final += getUnits(number);
            }
            else if (number < 20)
            {
                final += getValuesFrom10To19(number - 10);
            }
            else if (number < 100)
            {
                final += printInteger(number % 10, getTens(number / 10));
            }
            else if(number < 1000)
            {
                final += printInteger(number % 100, (getUnits(number / 100) + " hundred"));
            } else
            {
                final += printInteger(number % 1000, printInteger(number / 1000, "", i + 1));
                if (number % 1000 == 0)
                {
                    return final;
                }
            }
                

            return final + getNullCount(i);
        }

        public static string getValuesFrom10To19(int number)
        {
            switch (number)
            {
                case 0:
                    return "ten";
                    break;
                case 1:
                    return "eleven";
                    break;
                case 2:
                    return "twelve";
                    break;
                case 3:
                    return "thirteen";
                    break;
                case 4:
                    return "fourteen";
                    break;
                case 5:
                    return "fifteen";
                    break;
                case 6:
                    return "sixteen";
                    break;
                case 7:
                    return "seventeen";
                    break;
                case 8:
                    return "eighteen";
                    break;
                case 9:
                    return "nineteen";
                    break;
                default:
                    return "something wrong";
            }
        }

        public static string getTens(int number)
        {
            switch (number)
            {
                case 0:
                    return "";
                    break;
                case 2:
                    return "twenty";
                    break;
                case 3:
                    return "thirty";
                    break;
                case 4:
                    return "fourty";
                    break;
                case 5:
                    return "fifty";
                    break;
                case 6:
                    return "sixty";
                    break;
                case 7:
                    return "seventy";
                    break;
                case 8:
                    return "eighty";
                    break;
                case 9:
                    return "ninety";
                    break;
                default:
                    return "something wrong";
            }
        }

        public static string getUnits(int number)
        {
            switch (number)
            {
                case 0:
                    return "";
                    break;
                case 1:
                    return "one";
                    break;
                case 2:
                    return "two";
                    break;
                case 3:
                    return "three";
                    break;
                case 4:
                    return "four";
                    break;
                case 5:
                    return "five";
                    break;
                case 6:
                    return "six";
                    break;
                case 7:
                    return "seven";
                    break;
                case 8:
                    return "eight";
                    break;
                case 9:
                    return "nine";
                    break;
                default:
                    return "something wrong";
            }
        }

        public static string getNullCount(int index = 0)
        {
            switch (index)
            {
                case 0:
                    return "";
                    break;
                case 1:
                    return " thousand";
                    break;
                case 2:
                    return " million";
                    break;
                case 3:
                    return " billion";
                    break;
                default:
                    return " please enough";
            }
        }

        public static int enterIntegerFromConsole()
        {
            int number;
            do
            {
                Console.WriteLine("Please enter a number");
                if (int.TryParse(Console.ReadLine(), out number))
                    break;
                else
                    Console.WriteLine("Wrong input");
            } while (true);
            return number;
        }

        public static bool checkNegative(string number)
        {
            return number[0].Equals('-');
        }

        public static void checkPrintingNumber654()
        {
            if(printInteger(654).Equals("six hundred fifty four"))
                Console.WriteLine("Test OK");
            else
                Console.WriteLine("Test Failed");
        }

        public static void printFibonachiRow(int count = 0)
        {

            if (count == 0)
            {
                int[] fRow = new int[2];
                fRow[0] = 0;
                fRow[1] = 1;
                int temp = 0;
                while (true)
                {
                    temp = fRow[1] + fRow[0];
                    fRow[0] = fRow[1];
                    fRow[1] = temp;
                    Console.WriteLine(temp);
                }
            }
            else
            {
                int[] fRow = new int[count];
                fRow[0] = 0;
                fRow[1] = 1;
                for(int i = 2; i < count-1; i++)
                {
                    fRow[i] += fRow[i-2] + fRow[i-1];
                    Console.WriteLine(fRow[i]);
                }
            }
        }

        public static void printOdometrWithParameters(int odometrLength, int rotorLength)
        {
            int sum;
            for (int i = 0; i < (Math.Pow(rotorLength, odometrLength)); i++)
            {
                sum = i;
                for (int j = odometrLength - 1; j >= 0; j--)
                {
                    int temp = sum / (int)(Math.Pow(rotorLength,j));
                    Console.Write(temp);
                    sum -= (int)(Math.Pow(rotorLength, j)) * temp;
                }
                Console.WriteLine();
            }
        }
    }
}