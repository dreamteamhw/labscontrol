﻿namespace MyCalculator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button0 = new System.Windows.Forms.Button();
            this.deviationButton = new System.Windows.Forms.Button();
            this.multipleButton = new System.Windows.Forms.Button();
            this.sumButton = new System.Windows.Forms.Button();
            this.resultButton = new System.Windows.Forms.Button();
            this.numberField = new System.Windows.Forms.TextBox();
            this.divideButton = new System.Windows.Forms.Button();
            this.cButton = new System.Windows.Forms.Button();
            this.ceButton = new System.Windows.Forms.Button();
            this.removeButton = new System.Windows.Forms.Button();
            this.dotButton = new System.Windows.Forms.Button();
            this.changeSignButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 226);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(55, 52);
            this.button1.TabIndex = 0;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(74, 226);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(55, 52);
            this.button2.TabIndex = 1;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(135, 226);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(55, 52);
            this.button3.TabIndex = 2;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(13, 168);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(55, 52);
            this.button4.TabIndex = 3;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(74, 168);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(55, 52);
            this.button5.TabIndex = 4;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(135, 168);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(55, 52);
            this.button6.TabIndex = 5;
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(13, 110);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(55, 52);
            this.button7.TabIndex = 6;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(74, 110);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(55, 52);
            this.button8.TabIndex = 7;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(135, 110);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(55, 52);
            this.button9.TabIndex = 8;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button_Click);
            // 
            // button0
            // 
            this.button0.Location = new System.Drawing.Point(74, 284);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(55, 52);
            this.button0.TabIndex = 9;
            this.button0.Text = "0";
            this.button0.UseVisualStyleBackColor = true;
            this.button0.Click += new System.EventHandler(this.button_Click);
            // 
            // deviationButton
            // 
            this.deviationButton.Location = new System.Drawing.Point(196, 168);
            this.deviationButton.Name = "deviationButton";
            this.deviationButton.Size = new System.Drawing.Size(55, 52);
            this.deviationButton.TabIndex = 10;
            this.deviationButton.Text = "-";
            this.deviationButton.UseVisualStyleBackColor = true;
            this.deviationButton.Click += new System.EventHandler(this.Operation_Click);
            // 
            // multipleButton
            // 
            this.multipleButton.Location = new System.Drawing.Point(196, 110);
            this.multipleButton.Name = "multipleButton";
            this.multipleButton.Size = new System.Drawing.Size(55, 52);
            this.multipleButton.TabIndex = 11;
            this.multipleButton.Text = "*";
            this.multipleButton.UseVisualStyleBackColor = true;
            this.multipleButton.Click += new System.EventHandler(this.Operation_Click);
            // 
            // sumButton
            // 
            this.sumButton.Location = new System.Drawing.Point(196, 226);
            this.sumButton.Name = "sumButton";
            this.sumButton.Size = new System.Drawing.Size(55, 52);
            this.sumButton.TabIndex = 12;
            this.sumButton.Text = "+";
            this.sumButton.UseVisualStyleBackColor = true;
            this.sumButton.Click += new System.EventHandler(this.Operation_Click);
            // 
            // resultButton
            // 
            this.resultButton.Location = new System.Drawing.Point(196, 284);
            this.resultButton.Name = "resultButton";
            this.resultButton.Size = new System.Drawing.Size(55, 52);
            this.resultButton.TabIndex = 13;
            this.resultButton.Text = "=";
            this.resultButton.UseVisualStyleBackColor = true;
            this.resultButton.Click += new System.EventHandler(this.resultButton_Click);
            // 
            // numberField
            // 
            this.numberField.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberField.Location = new System.Drawing.Point(13, 13);
            this.numberField.Name = "numberField";
            this.numberField.Size = new System.Drawing.Size(238, 30);
            this.numberField.TabIndex = 14;
            this.numberField.Text = "0";
            this.numberField.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // divideButton
            // 
            this.divideButton.Location = new System.Drawing.Point(196, 52);
            this.divideButton.Name = "divideButton";
            this.divideButton.Size = new System.Drawing.Size(55, 52);
            this.divideButton.TabIndex = 15;
            this.divideButton.Text = "/";
            this.divideButton.UseVisualStyleBackColor = true;
            this.divideButton.Click += new System.EventHandler(this.Operation_Click);
            // 
            // cButton
            // 
            this.cButton.Location = new System.Drawing.Point(13, 52);
            this.cButton.Name = "cButton";
            this.cButton.Size = new System.Drawing.Size(55, 52);
            this.cButton.TabIndex = 16;
            this.cButton.Text = "C";
            this.cButton.UseVisualStyleBackColor = true;
            this.cButton.Click += new System.EventHandler(this.cButton_Click);
            // 
            // ceButton
            // 
            this.ceButton.Location = new System.Drawing.Point(74, 52);
            this.ceButton.Name = "ceButton";
            this.ceButton.Size = new System.Drawing.Size(55, 52);
            this.ceButton.TabIndex = 17;
            this.ceButton.Text = "CE";
            this.ceButton.UseVisualStyleBackColor = true;
            this.ceButton.Click += new System.EventHandler(this.ceButton_Click);
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(135, 52);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(55, 52);
            this.removeButton.TabIndex = 18;
            this.removeButton.Text = "<";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // dotButton
            // 
            this.dotButton.Location = new System.Drawing.Point(135, 284);
            this.dotButton.Name = "dotButton";
            this.dotButton.Size = new System.Drawing.Size(55, 52);
            this.dotButton.TabIndex = 19;
            this.dotButton.Text = ".";
            this.dotButton.UseVisualStyleBackColor = true;
            this.dotButton.Click += new System.EventHandler(this.button_Click);
            // 
            // changeSignButton
            // 
            this.changeSignButton.Location = new System.Drawing.Point(13, 284);
            this.changeSignButton.Name = "changeSignButton";
            this.changeSignButton.Size = new System.Drawing.Size(55, 52);
            this.changeSignButton.TabIndex = 20;
            this.changeSignButton.Text = "+/-";
            this.changeSignButton.UseVisualStyleBackColor = true;
            this.changeSignButton.Click += new System.EventHandler(this.changeSignButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(271, 344);
            this.Controls.Add(this.changeSignButton);
            this.Controls.Add(this.dotButton);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.ceButton);
            this.Controls.Add(this.cButton);
            this.Controls.Add(this.divideButton);
            this.Controls.Add(this.numberField);
            this.Controls.Add(this.resultButton);
            this.Controls.Add(this.sumButton);
            this.Controls.Add(this.multipleButton);
            this.Controls.Add(this.deviationButton);
            this.Controls.Add(this.button0);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.ShowIcon = false;
            this.Text = "Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.Button deviationButton;
        private System.Windows.Forms.Button multipleButton;
        private System.Windows.Forms.Button sumButton;
        private System.Windows.Forms.Button resultButton;
        private System.Windows.Forms.TextBox numberField;
        private System.Windows.Forms.Button divideButton;
        private System.Windows.Forms.Button cButton;
        private System.Windows.Forms.Button ceButton;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.Button dotButton;
        private System.Windows.Forms.Button changeSignButton;
    }
}