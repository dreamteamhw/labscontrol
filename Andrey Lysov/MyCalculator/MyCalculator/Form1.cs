﻿using System;
using System.Windows.Forms;

namespace MyCalculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private double firstValue { get; set; }
        private string operation { get; set; }
        private bool clickAfterOperation = false;

        public void addValue(string number)
        {
            if (clickAfterOperation)
                reset();
            addNumber(number);
            clickAfterOperation = false;
        }

        public double selectOperation(string operation)
        {
            switch (operation)
            {
                case "*":
                    return double.Parse(numberField.Text) * firstValue;
                    break;
                case "+":
                    return double.Parse(numberField.Text) + firstValue;
                    break;
                case "/":
                    return firstValue / double.Parse(numberField.Text);
                    break;
                case "-":
                    return firstValue - double.Parse(numberField.Text);
                    break;
                default:
                    return 0d;
                    break;
            }
        }

        public void addNumber(string number)
        {
            if (numberField.Text.Length <= Convert.ToString(double.MaxValue).Length)
            {
                if (numberField.Text == "0")
                    numberField.Text = number;
                else
                    numberField.Text += number;
            }
        }

        public void removeLastNumber()
        {
            if (numberField.Text.Length > 1)
                numberField.Text = numberField.Text.Substring(0, numberField.Text.Length - 1);
            else if (numberField.Text.Length == 1)
                numberField.Text = "0";
        }

        public void reset()
        {
            numberField.Text = "0";
        }

        private void button_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            addValue(b.Text);
        }

        private void cButton_Click(object sender, EventArgs e)
        {
            reset();
        }

        private void ceButton_Click(object sender, EventArgs e)
        {
            reset();
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            removeLastNumber();
        }

        private void Operation_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            firstValue = double.Parse(numberField.Text);
            operation = b.Text;
            clickAfterOperation = true;
        }

        private void resultButton_Click(object sender, EventArgs e)
        {
            clickAfterOperation = false;
            numberField.Text = Convert.ToString(selectOperation(operation));
        }

        private void changeSignButton_Click(object sender, EventArgs e)
        {
            if(numberField.Text[0] == '-')
            {
                numberField.Text = numberField.Text.Substring(1, numberField.Text.Length-1);
            } else if (numberField.Text == "0")
            {
            }
            else
            {
                numberField.Text = "-" + numberField.Text;
            }
        }
    }
}