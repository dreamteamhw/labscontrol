﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace letterCounter
{
    class Program
    {
        static void Main(string[] args)
        {
            lettersCounter(fileToString());
            Console.ReadLine();
        }

        public static string fileToString()
        {
            return File.ReadAllText(@"C:\Users\Andrey.Lysov\Desktop\HW2.txt");
        }

        public static void lettersCounter (string file)
        {
            int aCount = 0;
            int bCount = 0;
            int cCount = 0;
            int dCount = 0;
            int eCount = 0;
            int fCount = 0;
            int gCount = 0;
            int hCount = 0;
            int iCount = 0;
            int jCount = 0;
            int kCount = 0;
            int lCount = 0;
            int mCount = 0;
            int nCount = 0;
            int oCount = 0;
            int pCount = 0;
            int qCount = 0;
            int rCount = 0;
            int sCount = 0;
            int tCount = 0;
            int uCount = 0;
            int vCount = 0;
            int wCount = 0;
            int xCount = 0;
            int yCount = 0;
            int zCount = 0;
            int otherChar = 0;
            for (int i = 0; i < file.Length; i++)
            {
                switch (file[i])
                {
                    case 'a':
                    case 'A':
                        aCount++;
                        break;
                    case 'b':
                    case 'B':
                        bCount++;
                        break;
                    case 'c':
                    case 'C':
                        cCount++;
                        break;
                    case 'd':
                    case 'D':
                        dCount++;
                        break;
                    case 'e':
                    case 'E':
                        eCount++;
                        break;
                    case 'f':
                    case 'F':
                        fCount++;
                        break;
                    case 'g':
                    case 'G':
                        gCount++;
                        break;
                    case 'h':
                    case 'H':
                        hCount++;
                        break;
                    case 'i':
                    case 'I':
                        iCount++;
                        break;
                    case 'j':
                    case 'J':
                        jCount++;
                        break;
                    case 'k':
                    case 'K':
                        kCount++;
                        break;
                    case 'l':
                    case 'L':
                        lCount++;
                        break;
                    case 'm':
                    case 'M':
                        mCount++;
                        break;
                    case 'n':
                    case 'N':
                        nCount++;
                        break;
                    case 'o':
                    case 'O':
                        oCount++;
                        break;
                    case 'p':
                    case 'P':
                        pCount++;
                        break;
                    case 'q':
                    case 'Q':
                        qCount++;
                        break;
                    case 'r':
                    case 'R':
                        rCount++;
                        break;
                    case 't':
                    case 'T':
                        tCount++;
                        break;
                    case 's':
                    case 'S':
                        sCount++;
                        break;
                    case 'u':
                    case 'U':
                        uCount++;
                        break;
                    case 'v':
                    case 'V':
                        vCount++;
                        break;
                    case 'w':
                    case 'W':
                        wCount++;
                        break;
                    case 'x':
                    case 'X':
                        xCount++;
                        break;
                    case 'y':
                    case 'Y':
                        yCount++;
                        break;
                    case 'z':
                    case 'Z':
                        zCount++;
                        break;
                    default:
                        otherChar++;
                        break;
                }
            }
            Console.WriteLine("A\t" + aCount + "\nB\t" + bCount + "\nC\t" + cCount + "\nD\t" + dCount +
                "\nE\t" + eCount + "\nF\t" + fCount + "\nJ\t" + jCount + "\nH\t" + hCount +
                "\nI\t" + iCount + "\nG\t" + gCount + "\nK\t" + kCount + "\nL\t" + lCount +
                "\nM\t" + mCount + "\nN\t" + nCount + "\nO\t" + oCount + "\nP\t" + pCount +
                "\nQ\t" + qCount + "\nR\t" + rCount + "\nS\t" + sCount + "\nT\t" + tCount +
                "\nU\t" + uCount + "\nV\t" + vCount + "\nW\t" + wCount + "\nX\t" + xCount +
                "\nY\t" + yCount + "\nZ\t" + zCount + "\nOther\t" + otherChar);
        }
    }
}
