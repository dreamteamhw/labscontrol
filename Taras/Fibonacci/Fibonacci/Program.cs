﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fibonacci
{
    class Program
    {
        static int value(string welcomMsg)
        {
            Console.WriteLine(welcomMsg);
            string numbStr = Console.ReadLine();

            try
            {
                return int.Parse(numbStr);
            }
            catch
            {
                Console.WriteLine("Ошибка ввода. Попробуте снова");
                return value(welcomMsg);
            }
        }

        static void Main(string[] args)
        {
            int numb = value("Enter the last value:");

            int a = 1;
            int b = 1;

            while (a <= numb)
            {
                Console.WriteLine("Value is: " + a);
                b = a + b;
                a = b - a;
            }

            Console.ReadKey();
        }
    }
}
