﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberSpelling
{
    public class Program
    {
        static void LanguageChoosing(out string language)
        {
            Console.WriteLine("Choose language / Выберите язык: en/ru");
            string lang = Console.ReadLine();
            if ((lang == "en") || (lang == "EN") || (lang == "En"))
                language = "english";
            else if ((lang == "ru") || (lang == "RU") || (lang == "Ru"))
                language = "russian";
            else
            {
                Console.WriteLine("Wrong input, try again / Некорректный ввод, попробуйте снова");
                LanguageChoosing(out language);
            }
        }

        static void Input(out int x, string language)
        {
            if (language == "english")
            {
                Console.Write("Please, input the natural number: ");
                bool flag = int.TryParse(Console.ReadLine(), out x);
                if (!flag)
                {
                    Console.WriteLine("Wrong input, try again");
                    Input(out x, language);
                }
            }
            else if (language == "russian")
            {
                Console.Write("Пожалуйста, введите натуральное число: ");
                bool flag = int.TryParse(Console.ReadLine(), out x);
                if (!flag)
                {
                    Console.WriteLine("Некорректный ввод, попробуйте снова");
                    Input(out x, language);
                }
            }
            else x = 0;
        }

        public static string ConvertNumberToString(int x)
        {
            string result = "";
            bool minus = false;
            if (x < 0)
            {
                minus = true;
                x *= (-1);
            }
            if (x == 0)
                result = "zero";

            int hundred = x / 100 % 10;
            for (int i = 0; x > 0; ++i)
            {
                if ((i == 1) && (hundred == 0))
                    result = "and " + result;
                if (x % 1000 != 0)
                    result = HandleThreeDigits(x % 1000) + ' ' + dictionaryclasses[i] + ' ' + result;
                x /= 1000;
            }
            if (minus)
                result = "minus " + result;
            return DeleteUnneccecarySpacesInString(result);
        }

        static string DeleteUnneccecarySpacesInString(string s)
        {
            int firtsIndex = 0;
            int lastIndex;
            string result = "";
            int i = 0;
            while (s[i] == ' ')
                i++;
            firtsIndex = i;
            i = s.Length - 1;
            while (s[i] == ' ')
                i--;
            lastIndex = i;
            for (int j = firtsIndex; j <= lastIndex; ++j)
            {
                result += s[j];
            }
            return result;
        }

        public static string ConvertNumberToStringRu(int x)
        {
            string result = "";
            bool minus = false;
            if (x < 0)
            {
                minus = true;
                x *= (-1);
            }
            if (x == 0)
                result = "ноль";
            for (int i = 0; x > 0; ++i)
            {
                if (x % 1000 != 0)
                    result = HandleThreeDigitsRu(x % 1000, i) + result;
                x /= 1000;
            }
            if (minus)
                result = "минус" + result;
            return DeleteUnneccecarySpacesInString(result);
        }

        static string TwoDigitsNumber(int number)
        {
            string s = "";
            int d = number / 10;
            int e = number % 10;
            if (d == 1)
                s = dictionary11[e];
            else if (e == 0)
                s = dictionary10[d];
            else
                s = dictionary10[d] + '-' + dictionary1[e];
            return s;
        }

        static string ThreeDigitsNumber(int number)
        {
            string s = dictionary1[number / 100] + " hundred";
            if ((0 < number % 100) && (number % 100 < 10))
            {
                s += " and ";
                s += dictionary1[number % 100];
            }
            else if ((10 <= number % 100) && (number % 100 < 100))
            {
                s += " and ";
                s += TwoDigitsNumber(number % 100);
            }
            return s;
        }

        static string ThreeDigitsNumberRu(int number)
        {
            string s = dictionaryRu100[number / 100];
            //s += " ";
            int d = number / 10 % 10;
            int e = number % 10;
            if (d == 1)
                s += dictionaryRu11[e];
            else if (e == 0)
                s += dictionaryRu10[d];
            else
                s = s + dictionaryRu10[d] + dictionaryRu1[e];
            return s;
        }

        static string HandleThreeDigits(int number)
        {
            string s = "";
            if ((0 < number) && (number < 10)) s = dictionary1[number];
            if ((10 <= number) && (number < 100)) s = TwoDigitsNumber(number);
            if ((100 <= number) && (number < 1000)) s = ThreeDigitsNumber(number);
            return s;
        }

        static string HandleThreeDigitsRu(int number, int rank)
        {
            string s = "";
            if ((10 <= number % 100) && (number % 100 <= 19))
            {
                s = ThreeDigitsNumberRu(number) + dictionaryRuClasses[rank] + dictionaryClassEndings[rank, 0];

            }
            else
            {
                s = ThreeDigitsNumberRu(number) + dictionaryNumEndings[rank, number % 10] +
                                                  dictionaryRuClasses[rank] + dictionaryClassEndings[rank, number % 10];
            }
            return s;
        }


        static void Main(string[] args)
        {
            //ConvertNumberToStringTesting();
            //ConvertNumberToStringRuTesting();

            string language;
            LanguageChoosing(out language);

            int x;
            Input(out x, language);
            if (language == "english")
                Console.WriteLine("Your number is " + ConvertNumberToString(x));
            if (language == "russian")
                Console.WriteLine("Ваше число: " + ConvertNumberToStringRu(x));

            Console.ReadKey();
        }

        static string[] dictionary1 = new string[10] { "", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
        static string[] dictionary11 = new string[10] { "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen",
                                                        "seventeen", "eighteen", "nineteen" };
        static string[] dictionary10 = new string[10] { "", "", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety" };
        static string[] dictionaryclasses = new string[4] { "", "thousand", "million", "billion" };

        static string[] dictionaryRu1 = new string[10] { "", " од", " дв", " три", " четыре", " пять", " шесть", " семь", " восемь", " девять" };
        //static string[] dictionaryRu1Padezhi0 = new string[10] {"", "ин", "а", "", "", "", "", "", "", ""};
        //static string[] dictionaryRu1Padezhi1 = new string[10] { "", "на", "е", "", "", "", "", "", "", "" };

        static string[] dictionaryRu11 = new string[10] { " десять", " одиннадцать", " двенадцать", " тринадцать", " четырнадцать",
                                                          " пятнадцать", " шестнадцать", " семнадцать", " восемнадать", " девятнадцать" };
        static string[] dictionaryRu10 = new string[10] { "", "", " двадцать", " тридцать", " сорок", " пятьдесят", " шестьдесят",
                                                          " семьдесят", " восемьдесят", " девяносто" };
        static string[] dictionaryRu100 = new string[10] {"", " сто", " двести", " триста", " четыреста", " пятьсот", " шестьсот",
                                                          " семьсот", " восемьсот", " девятьсот"};
        //static string[] dictionaryRu1000 = new string[10] { "","а", "и", "и", "и", "", "", "", "", "" };
        //static string[] dictionaryRu1000000 = new string[10] { "ов", "", "а", "а", "а", "ов", "ов", "ов", "ов", "ов"};

        static string[] dictionaryRuClasses = new string[4] { "", " тысяч", " миллион", " миллиард" };

        static string[,] dictionaryNumEndings = new string[4, 10] { { "", "ин", "а", "", "", "", "", "", "", "" },
                                                                    { "", "на", "е", "", "", "", "", "", "", "" },
                                                                    { "", "ин", "а", "", "", "", "", "", "", "" },
                                                                    { "", "ин", "а", "", "", "", "", "", "", "" } };
        static string[,] dictionaryClassEndings = new string[4, 10] { { "", "", "", "", "", "", "", "", "", "" },
                                                                      { "","а", "и", "и", "и", "", "", "", "", "" },
                                                                      { "ов", "", "а", "а", "а", "ов", "ов", "ов", "ов", "ов"},
                                                                      { "ов", "", "а", "а", "а", "ов", "ов", "ов", "ов", "ов"} };
    }

}
