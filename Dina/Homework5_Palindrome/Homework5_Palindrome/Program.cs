﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework5_Palindrome
{
    class Program
    {
        static bool IsPalindrome (string str)
        {
            bool flag = true;
            for (int i = 0; i <= str.Length / 2; i++)
            {
                if (str[i] != str[str.Length - 1 - i])
                {
                    flag = false;
                }
            }
            return flag;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Input the string:");
            string s = Console.ReadLine();
            s = s.Replace(" ", "");
            s = s.ToLower();
            if (IsPalindrome(s))
            {
                Console.Write("this string is a palindrome");
            }
            else
            {
                Console.Write("this string is not a palindrome");
            }
            Console.ReadKey();
        }
    }
}
