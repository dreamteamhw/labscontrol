﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework3_Fibonacci
{
    class Program
    {
        static int Fibonacci(int number)
        {
            if (number == 0)
            {
                return 0;
            }
            else if (number == 1)
            {
                return 1;
            }
            else
            {
                return Fibonacci(number - 2) + Fibonacci(number - 1);
            }
        }

        static int Input(string message)
        {
            Console.Write(message);
            try
            {
                return int.Parse(Console.ReadLine());
            }
            catch
            {
                Console.WriteLine("Incorrect input. Try again");
                return Input(message);
            }
        }


        static void Main(string[] args)
        {
            int n = Input("How many Fibonacci numbers do you want to see? ");
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine(Fibonacci(i));
            }
            Console.ReadKey();
        }
    }
}
