﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework5_LoanCalculator
{
    class Program
    {
        static void PrintPaymentSchedule(double creditAmount, int months, double interest, DateTime startDate)
        {
            double percentPayment = interest * creditAmount;
            double partPayment = creditAmount / months;
            Console.WriteLine("{0,15}{1,15}{2,15}{3,15}{4,15}", "Date", "part", "percent", "total", "left");
            for (int i = 1; i <= months; i++)
            {
                DateTime paymentDay = startDate.AddMonths(i);
                creditAmount -= partPayment;
                Console.Write("{0,15}{1,15:F2}{2,15:F2}{3,15:F2}{4,15:F2}", 
                                paymentDay.ToShortDateString(), 
                                partPayment, 
                                percentPayment, 
                                partPayment + percentPayment, 
                                creditAmount);
                percentPayment = interest * creditAmount;
                Console.WriteLine();
            }            
        }

        static double InputDouble(string message)
        {
            double result = 0;
            Console.WriteLine(message);
            try
            {
                result = double.Parse(Console.ReadLine());
            }
            catch
            {
                Console.WriteLine("Something's wrong. Try again");
                result = InputDouble(message);
            }
            return result;
        }

        static int InputInt(string message)
        {
            int result = 0;
            Console.WriteLine(message);
            try
            {
                result = int.Parse(Console.ReadLine());
            }
            catch
            {
                Console.WriteLine("Something's wrong. Try again");
                result = InputInt(message);
            }
            return result;
        }


        static void Main(string[] args)
        {
            DateTime startDate = DateTime.Now;
            double creditAmount = InputDouble("How much money do you need? Input: ");
            int months = InputInt("How many months it will take you to return the loan? Input: ");
            double interest = InputDouble("Interest is: ") / 100;

            PrintPaymentSchedule(creditAmount, months, interest, startDate);
            Console.ReadKey();
        }
    }
}
