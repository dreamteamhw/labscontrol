﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework3_Odometer
{
    class Program
    {
        static string ConvertNumberFromDecimalToK(int number, int k)
        {
            string result = "";
            while (number >= k)
            {
                result = (number % k).ToString() + result;
                number /= k;
            }
            result = number.ToString() + result;
            return result;
        }

        static int Input(string message)
        {
            Console.Write(message);
            int result = 0;
            try
            {
                result = int.Parse(Console.ReadLine());
                if (result == 0 || result > 9)
                {
                    Console.WriteLine("Incorrect input. Try again");
                    return Input(message);
                }
                return result;
            }
            catch
            {
                Console.WriteLine("Incorrect input. Try again");
                return Input(message);
            }
        }
        static void Main(string[] args)
        {
            int k = Input("Input k: ");
            int i = 0;

            while (true)
            {
                Console.Clear();
                Console.WriteLine(ConvertNumberFromDecimalToK(i, k + 1));
                Console.ReadKey();
                i++;
            }
            //Console.ReadKey();
        }
    }
}