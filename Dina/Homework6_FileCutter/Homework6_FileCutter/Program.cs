﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework6_FileCutter
{
    class Program
    {
        static void CutFile(string filePath, int numberOfBytes, string destination)
        {
            FileInfo info = new FileInfo(filePath);
            FileStream myBigFile = File.Open(filePath, FileMode.Open);
            byte[] buffer = new byte[numberOfBytes];
            int readBytes = 0;
            int i = 0;
            while ((readBytes = myBigFile.Read(buffer, 0, numberOfBytes)) > 0)
            {
                string fileName = destination + info.Name + ".part" + i.ToString();
                FileStream resultFile = File.Create(fileName);
                resultFile.Write(buffer, 0, readBytes);
                resultFile.Close();
                i++;
            }
            myBigFile.Close();
        }

        static void MergeFile(string wantedFileName, string destination)
        {
            FileStream wantedFile = File.Create(destination + wantedFileName);
            int i = 0;
            string searchPart;
            while (File.Exists(searchPart = destination + wantedFileName + ".part" + i.ToString()))
            {
                FileStream wantedPart = File.Open(searchPart, FileMode.Open);
                FileInfo info = new FileInfo(searchPart);
                byte[] buffer = new byte[info.Length];
                int readBytes = wantedPart.Read(buffer, 0, (int)info.Length);
                wantedFile.Write(buffer, 0, readBytes);
                wantedPart.Close();
                i++;
            }
            wantedFile.Close();

        }
        static void Main(string[] args)
        {
            string destination = "C:\\Users\\DinaZavrik\\Documents\\MyResults\\";
            string filePath = @"C:\Users\DinaZavrik\Documents\Загрузки\Series\supernatural\Supernatural.(Season.1).Ukr.Rus.Eng\Supernatural.S01E03.DVDRip.Ukr.Rus.Eng.avi";
            int numberOfBytes = 1024 * 1024 * 50;
            CutFile(filePath, numberOfBytes, destination);
            string wantedFileName = "Supernatural.S01E03.DVDRip.Ukr.Rus.Eng.avi";
            MergeFile(wantedFileName, destination);

        }
    }
}
