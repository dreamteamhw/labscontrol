﻿using System;


namespace HomeWork1
{
    class Program
    {
        static void PrintNameInColors()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Dina");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Dina");
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("Dina");
            Console.BackgroundColor = ConsoleColor.DarkYellow;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("Dina");
            Console.BackgroundColor = ConsoleColor.Cyan;
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine("Dina");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
        }

        static void PrintStringBackwards(string input_string)
        {
            Console.WriteLine("This string backwards:");
            for (int i = input_string.Length - 1; i >= 0; --i)
            {
                Console.Write(input_string[i]);
            }
        }

        static void Numerals()
        {
            Console.WriteLine("Please, input the natural number no more than 2 147 483 647");
            string input = Console.ReadLine().ToString();
            //int N = input.Length;
            int x = int.Parse(input);
            string output = "";
            for (int i = 0; x > 0; ++i)
            {
                output = ThreeDigits(x % 1000) + ' ' + dictionaryclasses[i] + ' ' + output;
                x /= 1000;
            }
            if (output == "")
                output = "zero";
            Console.Write(output);
        }

        static string TwoDigitsNumber(int number)
        {
            string s = "";
            int d = number / 10;
            int e = number % 10;
            if (d == 1)
                s = dictionary11[e];
            else if (e == 0)
                s = dictionary10[d];
            else if (d == 0)
                s = dictionary1[e];
            else
                s = dictionary10[d] + '-' + dictionary1[e];            
            return s;
        }

        static string ThreeDigitsNumber(int number)
        {
            string s = "";
            s = dictionary1[number / 100] + " hundred";
            if (number % 100 != 0)
            {
                s += " ";
                s += TwoDigitsNumber(number % 100);
            }
            return s;
        }

        static string ThreeDigits(int number)
        {
            string s = "";
            if (number == 0) return s;
            if ((0 < number) && (number < 10)) s = dictionary1[number];
            if ((10 <= number) && (number < 100)) s = TwoDigitsNumber(number);
            if ((100 <= number) && (number < 1000)) s = ThreeDigitsNumber(number);
            return s;
        }


        static string[] dictionary1 = new string[10] { "", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
        static string[] dictionary11 = new string[10] { "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
        static string[] dictionary10 = new string[10] { "", "", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety" };
        static string[] dictionaryclasses = new string[4] { "", "thousand", "million", "billion" };


        static void Main(string[] args)
        {
            Console.WriteLine("1 part:");
            PrintNameInColors();
            Console.WriteLine();

            Console.WriteLine("2 part:");
            Console.WriteLine("Input the string");
            PrintStringBackwards(Console.ReadLine());
            Console.WriteLine();
            Console.WriteLine("3 part:");
            
            Numerals();
            Console.ReadKey();

        }
    }
}
