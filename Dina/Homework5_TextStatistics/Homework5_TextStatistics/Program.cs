﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework5_TextStatistics
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = "C:\\Users\\DinaZavrik\\Desktop\\MyFile.txt";
            string path2 = "C:\\Users\\DinaZavrik\\Downloads\\Пратчетт_-_Мрачный_Жнец.txt";
            string text = File.ReadAllText(path2, Encoding.Unicode);
            int[] statistics = new int[char.MaxValue];
            for (int i = 0; i < text.Length; i++)
            {
                char x = Char.ToLower(text[i]);
                statistics[x]++;
            }
            int total = 0;
            int different = 0;
            for (int i = 0; i < statistics.Length; i++)
            {
                if ((statistics[i] != 0) &&(!char.IsControl((char)i)))
                    //&& ((char)i != '\r') && ((char)i != '\n') && ((char)i != ' ')
                {
                    Console.WriteLine((char)i + " " + statistics[i] + " times");
                    different++;
                    total += statistics[i];
                }
            }
            Console.WriteLine("different " + different);
            Console.WriteLine("total " + total);
            Console.ReadKey();
        }
    }
}
