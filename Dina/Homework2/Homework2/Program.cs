﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework2
{
    class Program
    {
        static void Input(int count, out int x)
        {
            Console.Write("Введите " + count + " число: ");
            bool flag = int.TryParse(Console.ReadLine(), out x);
            if (!flag)
            {
                Console.WriteLine("Wrong input, try again");
                Input(count, out x);
            }
        }

        static long Add(int x, int y)
        {
            return x + y;
        }

        static long Substract(int x, int y)
        {
            return x - y;
        }

        static long Multiplicate(int x, int y)
        {
            return x * y;
        }

        static string TryDivide(int x, int y)
        {
            if (y == 0)
                return "Деление не может быть выполнено";
            else if (y > 0)
                return x + " / " + y + " = " + ((double)x / (double)y).ToString();
            else
                return x + " / (" + y + ") = " + ((double)x / (double)y).ToString();
        }

        static void Main(string[] args)
        {
            int x, y;
            Input(1, out x);
            Input(2, out y);
            string xString, yString;
            xString = x.ToString();
            if (y < 0)
                yString = "(" + y.ToString() + ")";
            else
                yString = y.ToString();
            Console.WriteLine(xString + " + " + yString + " = " + Add(x, y));
            Console.WriteLine(xString + " - " + yString + " = " + Substract(x, y));
            Console.WriteLine(xString + " * " + yString + " = " + Multiplicate(x, y));
            Console.WriteLine(TryDivide(x, y));
            Console.ReadKey();
        }
    }
}
