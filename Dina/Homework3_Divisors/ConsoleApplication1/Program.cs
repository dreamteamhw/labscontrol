﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework3_Divisors
{
    class Program
    {
        static int Input(string message)
        {
            Console.Write(message);
            int input = 0;
            try
            {
                input = int.Parse(Console.ReadLine());
            }
            catch
            {
                Console.WriteLine("Incorrect input. Try again");
                Input(message);
            }
            return input;
        }

        static bool IsDivisible(int number, int[] divisors)
        {
            bool isDivisible = true;
            for (int i = 0; i < divisors.Length; i++)
            {
                if (number % divisors[i] != 0)
                {
                    isDivisible = false;
                }
            }
            return isDivisible;

        }
        static void Main(string[] args)
        {
            Console.WriteLine("Эта программа выведет M чисел, кратных каждому из n введенных делителей");
            int amount = Input("Введите желаемое n - количество делителей: ");
            int[] divisors = new int[amount];
            for (int i = 0; i < divisors.Length; i++)
            {
                divisors[i] = Input("Веедите " + (i + 1).ToString() + " делитель: ");
            }
            int count = Input("Введите M - сколько кратных этим делителям Вы хотите найти: ");
            int integer = 1;
            while (count > 0)
            {
                if (IsDivisible(integer, divisors))
                {
                    Console.WriteLine(integer);
                    count--;
                }
                integer++;
            }
            Console.ReadKey();
        }
    }
}
