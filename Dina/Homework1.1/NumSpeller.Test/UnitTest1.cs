﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace NumSpeller.Test
{
    [TestClass]
    public class UnitTest1
    {

        public static bool ConvertNumberToStringTest(int number, string numeral)
        {    
            if (ConvertNumberToString(number) == numeral)
                return true;
            else
            {
                Console.WriteLine("Error!");
                Console.WriteLine("Expected: " + numeral);
                Console.WriteLine("Provided: " + ConvertNumberToString(number));
                return false;
            }
        }
        static void ConvertNumberToStringTesting()
        {
            ConvertNumberToStringTest(0, "zero");
            ConvertNumberToStringTest(-1, "minus one");
            ConvertNumberToStringTest(5, "five");
            ConvertNumberToStringTest(22, "twenty-two");
            ConvertNumberToStringTest(205, "two hundred and five");
            ConvertNumberToStringTest(1213, "one thousand two hundred and thirteen");
            ConvertNumberToStringTest(2002, "two thousand and two");
            ConvertNumberToStringTest(1234, "one thousand two hundred and thirty-four");
            ConvertNumberToStringTest(5013067, "five million thirteen thousand and sixty-seven");
            ConvertNumberToStringTest(13642030, "thirteen million six hundred and fourty-two thousand and thirty");
        }

        public static bool ConvertNumberToStringRuTest(int number, string numeral)
        {
            if (ConvertNumberToStringRu(number) == numeral)
                return true;
            else
            {
                Console.WriteLine("Error!");
                Console.WriteLine("Expected: " + numeral);
                Console.WriteLine("Provided: " + ConvertNumberToStringRu(number));
                return false;
            }
        }
        static void ConvertNumberToStringRuTesting()
        {
            ConvertNumberToStringRuTest(0, "ноль");
            ConvertNumberToStringRuTest(-1, "минус один");
            ConvertNumberToStringRuTest(5, "пять");
            ConvertNumberToStringRuTest(22, "двадцать два");
            ConvertNumberToStringRuTest(205, "двести пять");
            ConvertNumberToStringRuTest(1213, "одна тысяча двести тринадцать");
            ConvertNumberToStringRuTest(2002, "две тысячи два");
            ConvertNumberToStringRuTest(2234, "две тысячи двести тридцать четыре");
            ConvertNumberToStringRuTest(12004, "двенадцать тысяч четыре");
            ConvertNumberToStringRuTest(5013067, "пять миллионов тринадцать тысяч шестьдесят семь");
            ConvertNumberToStringRuTest(13642030, "тринадцать миллионов шестьсот сорок две тысячи тридцать");
        }


        [TestMethod]
        public void TestMethod1()
        {
        }
    }
}
