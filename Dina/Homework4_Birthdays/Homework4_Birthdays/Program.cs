﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework4_Birthdays
{
    class Program
    {
        static DateTime Input(string message)
        {
            DateTime input = new DateTime();
            Console.WriteLine(message);
            input = DateTime.Parse(Console.ReadLine());
            return input;
        }
        static void Main(string[] args)
        {
            DateTime birthday = new DateTime(1986, 10, 20);
            birthday = Input("Input your birthday dd/MM/yyyy");
            DateTime shiftedDate = birthday;
            int[] statistics = new int[7];
            for (int i = 0; i < 100; i++)
            {
                shiftedDate = shiftedDate.AddYears(i);
                int j = (int)shiftedDate.DayOfWeek;
                statistics[j] += 1;
            }
            for (int i = 0; i < statistics.Length; i++)
            {
                Console.Write((DayOfWeek)i + " ");
                Console.WriteLine(statistics[i]);
            }
            Console.ReadKey();
        }
    }
}
